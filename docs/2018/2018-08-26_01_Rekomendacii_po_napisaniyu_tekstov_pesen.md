## Рекомендации по написанию текстов песен
_(26 авг 2018)_

`#art`

(черновик)

- Ритмичность: текст должен быть выдержан по форме (количеству слогов, расположению ударений)

- Текст должен легко читаться: избегать труднопроизносимых сочетаний букв.

- Рифмы: избегать тривиальных рифм (напр. глагольных или часто встречающихся), отдавать предпочтение неявным рифмам.

- Легко запоминающиеся припевы (?)

- Припев - это кульминация, развитие или противопоставление куплету.

- Не должно быть строчек написанных "для заполнения", каждое слово должно иметь смысл.

- Отсылки к известным культурным событиям/феноменам, как правило, хороши.

- Метафоры и сравнения - полезный инструмент.

- Избегать избитых и сиюминутных тем (интернет, соцсети, прокрастинация)

- Текст песни должен нести позитивный посыл (даже если в целом настрой песни негативный)

- Не писать о себе лично.

- Текст строиться вокруг одной или нескольких ключевых идей. Попробуй оттолкнуться от одной фразы.

- В тексте должна присутствовать сюжетность (хотя сюжет в традиционном смысле может отсутствовать).

- Аспект новизны предельно важен.

- Писать о том, что тебе важно, интересно, в чем сам уверен (никаких драконов и единорогов)

- Нечрезмерная эмоциональность.

- Изобретение нестандартной формы - хорошо.

- Расширяй словарный запас 
