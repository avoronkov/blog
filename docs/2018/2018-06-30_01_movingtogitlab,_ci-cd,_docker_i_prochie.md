## #movingtogitlab, ci-cd, docker и прочие
_(30 июн 2018)_

`#movingtogitlab`

Все переезжают на гитлаб, я вот тоже присматриваюсь.
Оказывается тут есть клевый встроенный gitlab-ci, который можно использовать для публикации на gitlab pages.

Вообще, на этой неделе был на конференции Highload++ Siberia.
Там все говорят про docker и kubernetes, а также про continiuos integration/continious delivery,
ну и про то, что всё должно быть "автоматически" (с), по коммиту.
Ну и gitlab-ci позволяет потренироваться "на кошечках" :)

![cat](../image/20-cat.jpg)

P.S. На конференции не то, чтобы было особо интересно, но парочку докладов можно послушать.
Если меня не заломает как с кодефестом, то напишу небольшой обзорчик.
(на TEDx же я написал)) )
